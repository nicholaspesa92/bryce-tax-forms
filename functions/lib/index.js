"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
require('./app/config');
const api_1 = require("./app/api");
const config_1 = require("./app/config");
const functions = require("firebase-functions");
const sendg = require("@sendgrid/mail");
exports.app = api_1.api;
exports.newsgmail = functions.https.onRequest((req, res) => {
    config_1.cors(req, res, () => __awaiter(this, void 0, void 0, function* () {
        sendg.setApiKey(functions.config().sendgrid.key);
        const data = req.body.data;
        const companyName = data.companyName;
        const docUrl = data.docUrl;
        const healthDocs = data.healthDocs;
        const useremail = data.email;
        let mainDocBody = `
            <p>${companyName}: ${docUrl}</p>
            <br><span>Employee Health Docs</span><br><ul>
        `;
        healthDocs.map((doc) => {
            // tslint:disable:max-line-length
            try {
                mainDocBody += `<li>${doc.employeeName}: <a href="${doc.eeDoc}">${doc.eeDoc === '' ? 'Employee Portion Document: No Document' : 'Employee Portion Document'}</a> | <a href="${doc.erDoc}">${doc.erDoc === '' ? 'Employer Portion Document: No Document' : 'Employer Portion Document'}</a>`;
            }
            catch (err) {
                console.log(err);
            }
        });
        mainDocBody += `</ul>`;
        const msg = {
            to: ['payroll@taxnbooks.com', useremail],
            bcc: 'npesa1992@gmail.com',
            from: 'tammy@taxnbooks.com',
            subject: `${companyName} Tax Forms`,
            html: mainDocBody
        };
        console.log(msg);
        try {
            const email = yield sendg.send(msg);
            res.status(200).send({ text: email });
        }
        catch (err) {
            console.log(err);
            res.status(400).send({ text: err });
        }
    }));
});
exports.sender = functions.https.onRequest((req, res) => {
    config_1.cors(req, res, () => {
        const data = req.body.data;
        const companyName = data.companyName;
        const docUrl = data.docUrl;
        const healthDocs = data.healthDocs;
        console.log(data);
        const mainDocBody = `<p>${companyName}: ${docUrl}</p>`;
        const from = new config_1.sgmail.Email('npesa1992@gmail.com');
        const to = new config_1.sgmail.Email('npesa1992@gmail.com');
        const subject = `${companyName} Tax Forms`;
        const content = new config_1.sgmail.Content('text/html', mainDocBody);
        const mail = new config_1.sgmail.Mail(from, subject, to, content);
        const request = config_1.sg.emptyRequest({
            method: 'POST',
            path: '/v3/mail/send',
            body: mail.toJSON()
        });
        config_1.sg.API(request, (error, response) => {
            if (error) {
                console.log(error);
                return res.status(403).send({ text: error });
            }
            return res.status(200).send({ text: res });
        });
    });
});
//# sourceMappingURL=index.js.map