"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const functions = require("firebase-functions");
const helpers = require("./helpers");
const config_1 = require("./config");
config_1.app.post('/send', (req, res) => {
    console.log(req.body);
    const userId = req.user.uid;
    const data = req.body.data;
    const promise = helpers.sendEmail(userId, data);
    defaultHandler(promise, res);
});
function defaultHandler(promise, res) {
    promise
        .then(data => res.status(200).send(data))
        .catch(error => res.status(400).send({ text: error }));
}
exports.api = functions.https.onRequest(config_1.app);
//# sourceMappingURL=api.js.map