"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const config_1 = require("./config");
function authenticateUser(req, res, next) {
    let authToken;
    if (req.headers.authorization && req.headers.authorization.startsWith('Bearer')) {
        authToken = req.headers.authorization.split('Bearer ')[1];
    }
    else {
        res.status(403).send({ text: 'Must provide a header that looks like "Authorization: Bearer <Firebase ID Token>"' });
    }
    config_1.auth.verifyIdToken(authToken)
        .then(decodedToken => {
        req.user = decodedToken;
    }).catch((error) => {
        res.status(403).send(error);
    });
}
exports.authenticateUser = authenticateUser;
function sendEmail(userId, data) {
    return __awaiter(this, void 0, void 0, function* () {
        const companyName = data.companyName;
        const docUrl = data.docUrl;
        const healthDocs = data.healthDocs;
        console.log(healthDocs);
        const mainDocBody = `<p>${companyName}: ${docUrl}</p>`;
        const from = new config_1.sgmail.Email('npesa1992@gmail.com');
        const to = new config_1.sgmail.Email('npesa1992@gmail.com');
        const subject = `${companyName} Tax Forms`;
        const content = new config_1.sgmail.Content('text/html', mainDocBody);
        const mail = new config_1.sgmail.Mail(from, subject, to, content);
        const req = config_1.sg.emptyRequest({
            method: 'POST',
            path: '/v3/mail/send',
            body: mail.toJSON()
        });
        config_1.sg.API(req, (error, res) => {
            if (error) {
                console.log(error);
                return error;
            }
            return res;
        });
    });
}
exports.sendEmail = sendEmail;
//# sourceMappingURL=helpers.js.map