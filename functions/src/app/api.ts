import * as functions from 'firebase-functions';
import * as helpers from './helpers';
import { app } from './config';

app.post('/send', (req: any, res: any) => {
    console.log(req.body);
    const userId = req.user.uid;
    const data = req.body.data;
    const promise = helpers.sendEmail(userId, data);
    defaultHandler(promise, res);
});

function defaultHandler(promise: Promise<any>, res: any): void {
    promise
        .then(data => res.status(200).send(data))
        .catch(error => res.status(400).send({ text: error }));
}

export const api = functions.https.onRequest(app);
