import { auth, sgmail, sg } from './config';

export function authenticateUser(req, res, next): void {
    let authToken;

    if (req.headers.authorization && req.headers.authorization.startsWith('Bearer')) {
        authToken = req.headers.authorization.split('Bearer ')[1];
    } else {
        res.status(403).send({ text: 'Must provide a header that looks like "Authorization: Bearer <Firebase ID Token>"' });
    }

    auth.verifyIdToken(authToken)
        .then(decodedToken => {
            req.user = decodedToken;
        }).catch((error) => {
            res.status(403).send(error);
        });
}

export async function sendEmail(userId, data) {
    const companyName = data.companyName;
    const docUrl = data.docUrl;
    const healthDocs = data.healthDocs;
    console.log(healthDocs);

    const mainDocBody = `<p>${companyName}: ${docUrl}</p>`;

    const from = new sgmail.Email('npesa1992@gmail.com');
    const to = new sgmail.Email('npesa1992@gmail.com');
    const subject = `${companyName} Tax Forms`;

    const content = new sgmail.Content('text/html', mainDocBody);
    const mail = new sgmail.Mail(from, subject, to, content);

    const req = sg.emptyRequest({
        method: 'POST',
        path: '/v3/mail/send',
        body: mail.toJSON()
    });

    sg.API(req, (error, res) => {
        if (error) { console.log(error); return error; }
        return res;
    });
}
