require('./app/config');
import { api } from './app/api';
import { auth, sgmail, sg, cors } from './app/config';
import * as functions from 'firebase-functions';
import * as sendg from '@sendgrid/mail';

export const app = api;

exports.newsgmail = functions.https.onRequest((req, res) => {
    cors(req, res, async () => {
        sendg.setApiKey(functions.config().sendgrid.key);

        const data = req.body.data;
        const companyName = data.companyName;
        const docUrl = data.docUrl;
        const healthDocs = data.healthDocs;
        const useremail = data.email;

        let mainDocBody =
        `
            <p>${companyName}: ${docUrl}</p>
            <br><span>Employee Health Docs</span><br><ul>
        `;
        healthDocs.map((doc) => {
            // tslint:disable:max-line-length
            try {
                mainDocBody += `<li>${doc.employeeName}: <a href="${doc.eeDoc}">${doc.eeDoc === '' ? 'Employee Portion Document: No Document' : 'Employee Portion Document'}</a> | <a href="${doc.erDoc}">${doc.erDoc === '' ? 'Employer Portion Document: No Document' : 'Employer Portion Document'}</a>`;
            } catch (err) {
                console.log(err);
            }
        });
        mainDocBody += `</ul>`;

        const msg = {
            to: ['payroll@taxnbooks.com', useremail],
            bcc: 'npesa1992@gmail.com',
            from: 'tammy@taxnbooks.com',
            subject: `${companyName} Tax Forms`,
            html: mainDocBody
        };
        console.log(msg);
        try {
            const email = await sendg.send(msg);
            res.status(200).send({ text: email });
        } catch (err) {
            console.log(err);
            res.status(400).send({ text: err});
        }
    });
});

exports.sender = functions.https.onRequest((req, res) => {
    cors(req, res, () => {
        const data = req.body.data;
        const companyName = data.companyName;
        const docUrl = data.docUrl;
        const healthDocs = data.healthDocs;
        console.log(data);

        const mainDocBody = `<p>${companyName}: ${docUrl}</p>`;

        const from = new sgmail.Email('npesa1992@gmail.com');
        const to = new sgmail.Email('npesa1992@gmail.com');
        const subject = `${companyName} Tax Forms`;

        const content = new sgmail.Content('text/html', mainDocBody);
        const mail = new sgmail.Mail(from, subject, to, content);

        const request = sg.emptyRequest({
            method: 'POST',
            path: '/v3/mail/send',
            body: mail.toJSON()
        });

        sg.API(request, (error, response) => {
            if (error) { console.log(error); return res.status(403).send({text: error}); }
            return res.status(200).send({ text: res });
        });
    });
});
