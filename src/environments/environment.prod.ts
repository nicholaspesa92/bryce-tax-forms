export const environment = {
  production: true,
  firebase: {
    apiKey: 'AIzaSyDt7qlphWONZ8Y4zmXo7IQY6avT_njxVds',
    authDomain: 'taxnbookspayroll.firebaseapp.com',
    databaseURL: 'https://taxnbookspayroll.firebaseio.com',
    projectId: 'taxnbookspayroll',
    storageBucket: 'taxnbookspayroll.appspot.com',
    messagingSenderId: '855975065268'
  },
  functionsUrl: 'https://us-central1-taxnbookspayroll.cloudfunctions.net/app'
};
