import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Router } from '@angular/router';
import { DomSanitizer } from '@angular/platform-browser';
import { FormBuilder, FormGroup, FormArray, Validators } from '@angular/forms';
import { CurrencyPipe } from '@angular/common';
import { PayrollService } from './../ng-services/payroll.service';
import { AuthService } from './../ng-services/auth.service';
import * as firebase from 'firebase';

import * as pdfMake from 'pdfmake/build/pdfmake';
import * as pdfFonts from 'pdfmake/build/vfs_fonts';

@Component({
  selector: 'app-payroll',
  templateUrl: './payroll.component.html',
  styleUrls: ['./payroll.component.scss']
})
export class PayrollComponent implements OnInit {
  // tslint:disable:max-line-length

  logo: any;

  payrollForm: FormGroup;

  typeRadios: any[];
  payRadios: any[];
  deductionRadios: any[];

  empForm: FormGroup;
  employee: FormGroup;
  deduction: FormGroup;

  emps: number;

  dd: any;
  styles: any;

  user: any;

  eeUp: boolean;
  erUp: boolean;
  percent;

  tab: number;

  pd: any;
  ed: any;

  sending: boolean;
  submitted: boolean;

  constructor(
    private router: Router,
    private sani: DomSanitizer,
    private fb: FormBuilder,
    private cdr: ChangeDetectorRef,
    private cp: CurrencyPipe,
    private ps: PayrollService,
    private as: AuthService
  ) {
    this.emps = -1;
    this.eeUp = false;
    this.erUp = false;
    this.percent = 0;
    this.tab = 0;
    this.submitted = false;
    this.sending = false;
    this.logo = this.sani.bypassSecurityTrustResourceUrl('https://firebasestorage.googleapis.com/v0/b/bryce-tax-forms.appspot.com/o/assets%2FLogo1.829.png?alt=media&token=e1d91e30-614f-4513-b24c-067885209644');
    pdfMake.vfs = pdfFonts.pdfMake.vfs;
    this.styles = {
      docTitle: {
        fontSize: 24,
        marginBottom: 24
      },
      tableStyle: {
        marginBottom: 12
      },
      tableHeader: {
        fontSize: 8
      },
      tableRow: {
        fontSize: 10
      },
      sectionHeader: {
        fontSize: 16,
        marginTop: 16,
        marginBottom: 12
      },
      empHeader: {
        fontSize: 12
      },
      empTable: {
        marginTop: 4,
        marginBottom: 4
      },
      additionalPayHeader: {
        fontSize: 10,
        alignment: 'center',
        marginBottom: 4,
        marginTop: 4
      },
      gap: {
        marginTop: 16,
        marginBottom: 16,
        background: 'black'
      },
      footer: {
        fontSize: 8,
        marginLeft: 48
      }
    };
    this.dd = {
      content: [],
      styles: this.styles,
      footer: ((p, c) => {
        return { text: p.toString() + ' of ' + c, style: 'footer' };
      })
    };
  }

  ngOnInit() {
    this.buildForm();
    this.buildRadios();
    this.buildEmpForm();
    this.anon();
  }

  async anon() {
    this.as.user.subscribe((user) => { user ? this.user = user : this.as.anonSignin(); });
  }

  buildForm() {
    this.payrollForm = this.fb.group({
      companyName: ['', Validators.required],
      payPeriodStart: [new Date(), Validators.required],
      payPeriodEnd: [new Date(), Validators.required],
      payDate: [new Date(), Validators.required],
      checkDate: [new Date(), Validators.required],
      payType: ['office-checks', Validators.required],
      checkNumber: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]]
    });
    this.pd = this.payrollForm.value;
  }

  buildRadios() {
    this.typeRadios = [
      { text: 'We write paychecks', value: 'office-checks' },
      { text: 'Direct Deposit', value: 'direct-deposit' },
      { text: 'Taxnbooks prints my paychecks', value: 'taxnbooks-checks' },
      { text: 'We write paychecks and direct deposit', value: 'office-direct-checks' }
    ];
    this.payRadios = [
      { text: 'Bonus', value: 'bonus' },
      { text: 'Commission', value: 'commission' },
      { text: 'Holiday', value: 'holiday' },
      { text: 'Personal Time Off', value: 'personal-time' },
      { text: 'Piece Work', value: 'piece-work' },
      { text: 'Sick Time', value: 'sick-time' },
      { text: 'Tips', value: 'tips' },
      { text: 'Vacation', value: 'vacation' }
    ];
    this.deductionRadios = [
      { text: 'Garnishment', value: 'garnishment' },
      { text: 'Pay Advance', value: 'pay-advance' },
      { text: 'Tips Paid Out', value: 'tips' },
      { text: 'Simple IRA Contribution', value: 'ira-contribution' },
      { text: '401(k) Contrbution', value: '401k-contribution' },
      { text: 'HSA Contribution', value: 'hsa-contribution' },
      { text: 'Aflac Contribution', value: 'aflac-contribution' }
    ];
  }

  buildEmpForm() {
    this.empForm = this.fb.group({
      employees: this.fb.array([])
    });
    this.ed = this.empForm.value;
  }

  addEmployeeToForm() {
    this.emps += 1;
    const ctrl = <FormArray>this.empForm.controls['employees'];
    const emp = this.employeeBlock();
    ctrl.push(emp);
    this.cdr.detectChanges();
  }

  removeEmployeeFromForm(i) {
    this.emps -= 1;
    const ctrl = <FormArray>this.empForm.controls['employees'];
    ctrl.removeAt(i);
  }

  addPaySourceToForm(i, emp) {
    const ctrl = <FormArray>emp.controls['additionalPay'].controls['paySources'];
    const ps = this.payBlock();
    ctrl.push(ps);
  }

  removePaySourceFromForm(pi, emp) {
    const ctrl = <FormArray>emp.controls['additionalPay'].controls['paySources'];
    ctrl.removeAt(pi);
  }

  addDeductionToForm(i, emp) {
    const ctrl = <FormArray>emp.controls['deduc'].controls['deductions']; // <FormArray>this.empForm.controls['employees'].value[i].deduc['deductions'];
    const ded = this.deductionBlock();
    ctrl.push(ded);
  }

  removeDeductionFromForm(di, emp) {
    const ctrl = <FormArray>emp.controls['deduc'].controls['deductions'];
    ctrl.removeAt(di);
  }

  employeeBlockX() {
    return this.fb.group({
      employeeName: ['', Validators.required],
      regHours: [0, Validators.required],
      overtimeHours: [0, Validators.required],
      holidayHours: [0, Validators.required],
      sickHours: [0, Validators.required],
      regWage: [0, Validators.required],
      overtimeWage: [0, Validators.required],
      salary: [0, Validators.required],
      pieceRate: [0, Validators.required],
      commission: [0, Validators.required],
      bonus: [0, Validators.required],
      payRate: ['', Validators.required],
      deduc: this.fb.group({
        deductions: this.fb.array([])
      }),
      employeeProvidedInsurance: [false, Validators.required],
      officeProvidedInsurance: [false, Validators.required]
    });
  }

  employeeBlock() {
    return this.fb.group({
      employeeName: ['', Validators.required],
      typeOfPay: ['hourly', Validators.required],
      hourlyPayRate: [0],
      hourlyPayHours: [0],
      overtimePayHours: [0],
      salary: [0],
      workmansCompCode: [''],
      additionalPay: this.fb.group({
        paySources: this.fb.array([])
      }),
      deduc: this.fb.group({
        deductions: this.fb.array([])
      }),
      shareholderHealthInsurance: [0, Validators.required],
      employeePortionHealthInsurance: [0, Validators.required],
      employeePortionDocumentUrl: [''],
      employerPortionHealthInsurance: [0, Validators.required],
      employerPortionDocumentUrl: [''],
      notes: ['']
    });
  }

  payBlock() {
    return this.fb.group({
      type: [''],
      hours: [0],
      hourlyPayRate: [0],
      otherPayAmount: [0],
      workmansCompCode: ['']
    });
  }

  deductionBlock() {
    return this.fb.group({
      type: ['', Validators.required],
      amount: [0, Validators.required]
    });
  }

  inputChange(el: HTMLInputElement|any) {
    el.target.value = this.cp.transform(el.target.value, 'USD', 'symbol', '1.0-2');
  }

  review() {
    this.pd = this.payrollForm.value;
    this.ed = this.empForm.value;
    this.tab === 0 ? this.tab += 1 : this.tab -= 1;
  }

  tabChange(e) {
    this.tab = e.index;
    this.pd = this.payrollForm.value;
    this.ed = this.empForm.value;
  }

  buildDoc() {
    this.dd = this.ps.buildDocument(this.pd, this.ed);
    const doc = pdfMake.createPdf(this.dd);
    doc.getDataUrl((dataUrl) => {
      this.setFileAndSend(dataUrl, `submissions/${this.user.uid}`);
    });
  }

  async setFileAndSend(string, path) {
    this.sending = true;
    const docUrl = await this.ps.uploadDataUrl(string, path);
    const healthDocs = [];
    this.ed.employees.map((employee) => {
      const item: any = {
        employeeName: employee.employeeName,
        eeDoc: '',
        erDoc: ''
      };
      if (employee.employeePortionDocumentUrl !== '') { item.eeDoc = employee.employeePortionDocumentUrl.url; }
      if (employee.employerPortionDocumentUrl !== '') { item.erDoc = employee.employerPortionDocumentUrl.url; }
      healthDocs.push(item);
    });
    const body = {
      data: {
        companyName: this.pd.companyName,
        docUrl: docUrl,
        healthDocs: healthDocs,
        email: this.pd.email
      }
    };
    console.log(body);
    const req = this.ps.sendDoc(body);
    req.subscribe((res) => {
      this.sending = false;
      console.log(res);
      this.submitted = true;
    });
  }

  download() {
    this.dd = this.ps.buildDocument(this.pd, this.ed);
    const doc = pdfMake.createPdf(this.dd);
    doc.download();
  }

  async uploadHealthDoc(e, field, emp) {
    if (e.target.files.length > 0) {
      field === 'employeePortionDocumentUrl' ? this.eeUp = true : this.erUp = true;
      const url = await this.uploadInputFile(e.target.files[0], `insuranceDocs/${this.user.uid}_${emp.employeeName}_${field}`);
      const name = e.target.files[0].name;
      const doc = { name: name, url: url };
      const temp = {};
      temp[field] = doc;
      emp.patchValue(temp);
      this.percent = 0;
      field === 'employeePortionDocumentUrl' ? this.eeUp = false : this.erUp = false;
    }
  }

  uploadDataUrl(string, path) {
    return new Promise((resolve, reject) => {
      const sRef = firebase.storage().ref();
      const uRef = sRef.child(`${path}`);
      const uTask = uRef.putString(string, 'data_url');
      uTask.on(firebase.storage.TaskEvent.STATE_CHANGED,
        (snapshot: firebase.storage.UploadTaskSnapshot) => {
          // console.log((snapshot.bytesTransferred / snapshot.totalBytes) * 100);
        },
        (error) => {
          reject(error);
        },
        async () => {
          const u = await uTask.snapshot.ref.getDownloadURL();
          resolve(u);
        }
      );
    });
  }

  uploadInputFile(file, path) {
    return new Promise((resolve, reject) => {
      const sRef = firebase.storage().ref();
      const uRef = sRef.child(path);
      const uTask = uRef.put(file);
      uTask.on(firebase.storage.TaskEvent.STATE_CHANGED,
        (snapshot: firebase.storage.UploadTaskSnapshot) => {
          console.log((snapshot.bytesTransferred / snapshot.totalBytes) * 100);
          this.percent = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
        },
        (error) => {
          reject(error);
        },
        async () => {
          const u = await uTask.snapshot.ref.getDownloadURL();
          resolve(u);
        }
      );
    });
  }

}
