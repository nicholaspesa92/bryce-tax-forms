import { Injectable } from '@angular/core';
import * as firebase from 'firebase';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { HttpClient } from '@angular/common/http';
import { environment } from './../../environments/environment';
import { DecimalPipe } from '@angular/common';

@Injectable({
  providedIn: 'root'
})
export class PayrollService {

  _payrollData: any;
  _employeeData: any;

  dd: any;
  styles: any;

  percentSubject: BehaviorSubject<number> = new BehaviorSubject(0);

  baseUrl: any;

  constructor(
    private http: HttpClient,
    private dp: DecimalPipe
  ) {
    this.baseUrl = environment.functionsUrl;
    this.styles = {
      docTitle: {
        fontSize: 24,
        marginBottom: 24
      },
      tableStyle: {
        marginBottom: 12
      },
      tableHeader: {
        fontSize: 8
      },
      tableRow: {
        fontSize: 10
      },
      sectionHeader: {
        fontSize: 16,
        marginTop: 16,
        marginBottom: 12
      },
      sectionHeaderRight: {
        fontSize: 16,
        marginTop: 16,
        marginBottom: 12,
        alignment: 'right'
      },
      empHeader: {
        fontSize: 12
      },
      empTable: {
        marginTop: 4,
        marginBottom: 4
      },
      additionalPayHeader: {
        fontSize: 10,
        alignment: 'center',
        marginBottom: 4,
        marginTop: 4
      },
      gap: {
        marginTop: 16,
        marginBottom: 16,
        background: 'black'
      },
      footer: {
        fontSize: 8,
        marginLeft: 48
      }
    };
    this.dd = {
      content: [],
      styles: this.styles,
      footer: ((p, c) => {
        return { text: p.toString() + ' of ' + c, style: 'footer' };
      })
    };
  }

  set payrollData(data) {
    this._payrollData = data;
  }

  get payrollData() {
    return this._payrollData;
  }

  set employeeData(data) {
    this._employeeData = data;
  }

  get employeeData() {
    return this._employeeData;
  }

  sendDoc(body) {
    return this.http.post(`https://us-central1-taxnbookspayroll.cloudfunctions.net/newsgmail`, body);
  }

  uploadDataUrl(string, path) {
    return new Promise((resolve, reject) => {
      const sRef = firebase.storage().ref();
      const uRef = sRef.child(`${path}`);
      const uTask = uRef.putString(string, 'data_url');
      uTask.on(firebase.storage.TaskEvent.STATE_CHANGED,
        (snapshot: firebase.storage.UploadTaskSnapshot) => {
          this.percentSubject.next((snapshot.bytesTransferred / snapshot.totalBytes) * 100);
        },
        (error) => {
          reject(error);
        },
        async () => {
          const u = await uTask.snapshot.ref.getDownloadURL();
          resolve(u);
        }
      );
    });
  }

  buildDocument(p, e) {
    this.dd.content = [
      { text: p.companyName, style: 'docTitle' },
      {
        table: {
          widths: [ '*', '*', '*' ],
          body: [
            [
              { text: 'Pay Period Start', style: 'tableHeader' },
              { text: 'Pay Period End', style: 'tableHeader' },
              { text: 'Pay Date', style: 'tableHeader' }
            ],
            [
              { text: p.payPeriodStart.toDateString(), style: 'tableRow' },
              { text: p.payPeriodEnd.toDateString(), style: 'tableRow' },
              { text: p.payDate.toDateString(), style: 'tableRow' }
            ]
          ]
        },
        layout: {
          hLineWidth: ((i, node) => i === 1 ? 1 : 0),
          vLineWidth: ((i, node) => 0)
        },
        style: 'tableStyle'
      },
      {
        table: {
          widths: [ '*', '*', '*' ],
          body: [
            [
              { text: 'Form of Pay', style: 'tableHeader' },
              { text: 'Check #', style: 'tableHeader' },
              { text: 'Email', style: 'tableHeader' }
            ],
            [
              { text: p.payType, style: 'tableRow' },
              { text: p.checkNumber, style: 'tableRow' },
              { text: p.email, style: 'tableRow' }
            ]
          ]
        },
        layout: {
          hLineWidth: ((i, node) => i === 1 ? 1 : 0),
          vLineWidth: ((i, node) => 0)
        },
        style: 'tableStyle'
      },
      // { text: 'Employees', style: 'sectionHeader' }
      {
        columns: [
          { text: 'Employees', style: 'sectionHeader' },
          { text: e.employees.length + ' Employees', style: 'sectionHeaderRight' }
        ]
      }
    ];
    this.createEmpTables(e.employees);
    return this.dd;
  }

  createEmpTables(emps) {
    const small = 38;
    const medium = 56;
    const large = 64;
    const xlarge = 72;
    const xxlarge = 88;
    emps.map((emp, index) => {
      this.dd.content.push({ text: `Employee ${index + 1}`, style: 'empHeader'});
      const empRow: any = {
        table: {
          widths: [
            '*', // Name
            large, // Type (Hourly/Salary)
          ],
          body: [
            [
              { text: 'Employee Name', style: 'tableHeader' },
              { text: 'Pay Type', style: 'tableHeader' }
            ]
          ]
        },
        style: 'empTable'
      };
      if (emp.typeOfPay === 'hourly') {
        empRow.table.widths.push(large);
        empRow.table.widths.push(large);
        empRow.table.widths.push(large);
        empRow.table.widths.push(xxlarge);
        empRow.table.body[0].push({ text: 'Hourly Rate', style: 'tableHeader' });
        empRow.table.body[0].push({ text: 'Hourly Hours', style: 'tableHeader' });
        empRow.table.body[0].push({ text: 'Overtime Hours', style: 'tableHeader' });
        empRow.table.body[0].push({ text: 'Workers Comp Code', style: 'tableHeader' });

        const hPay = [
          { text: emp.employeeName, style: 'tableRow' },
          { text: emp.typeOfPay, style: 'tableRow' },
          { text: this.dec(emp.hourlyPayRate), style: 'tableRow' },
          { text: this.dec(emp.hourlyPayHours), style: 'tableRow' },
          { text: this.dec(emp.overtimePayHours), style: 'tableRow'},
          { text: emp.workmansCompCode, style: 'tableRow' }
        ];
        empRow.table.body.push(hPay);
      } else {
        empRow.table.widths.push(large);
        empRow.table.widths.push(xxlarge);
        empRow.table.body[0].push({ text: 'Salary Pay', style: 'tableHeader' });
        empRow.table.body[0].push({ text: 'Workers Comp Code', style: 'tableHeader' });

        const sPay = [
          { text: emp.employeeName, style: 'tableRow' },
          { text: emp.typeOfPay, style: 'tableRow' },
          { text: this.dec(emp.salary), style: 'tableRow' },
          { text: emp.workmansCompCode, style: 'tableRow' }
        ];
        empRow.table.body.push(sPay);
      }
      this.dd.content.push(empRow);

      if (emp.additionalPay.paySources.length > 0) {
        this.dd.content.push({ text: 'Additional Pay', style: 'additionalPayHeader' });
        const adPayTable: any = {
          table: {
            widths: ['*', medium, medium, xlarge, '*'],
            body: [
              [
                { text: 'Type of Pay', style: 'tableHeader' },
                { text: 'Hours', style: 'tableHeader' },
                { text: 'Hourly Rate', style: 'tableHeader' },
                { text: 'Other (Not Hourly)', style: 'tableHeader' },
                { text: 'Workers Comp Code', style: 'tableHeader' }
              ]
            ]
          }
        };
        emp.additionalPay.paySources.map((source, pi) => {
          const pay = [
            { text: source.type, style: 'tableRow' },
            { text: this.dec(source.hours), style: 'tableRow' },
            { text: this.dec(source.hourlyPayRate), style: 'tableRow' },
            { text: this.dec(source.otherPayAmount), style: 'tableRow' },
            { text: source.workmansCompCode, style: 'tableRow' },
          ];
          adPayTable.table.body.push(pay);
        });
        this.dd.content.push(adPayTable);
      }

      if (emp.deduc.deductions.length > 0) {
        this.dd.content.push({ text: 'Deductions', style: 'additionalPayHeader' });
        const deductionTable: any = {
          table: {
            widths: ['*', '*'],
            body: [
              [
                { text: 'Type of Deduction', style: 'tableHeader' },
                { text: 'Amount / Hours', style: 'tableHeader' }
              ]
            ]
          }
        };
        emp.deduc.deductions.map((source, di) => {
          const deduc = [
            { text: source.type, style: 'tableRow' },
            { text: this.dec(source.amount), style: 'tableRow' }
          ];
          deductionTable.table.body.push(deduc);
        });
        this.dd.content.push(deductionTable);
      }

      this.dd.content.push({ text: 'Health Insurance Information', style: 'additionalPayHeader' });
      const healthTable: any = {
        table: {
          widths: ['*', '*', '*', '*', '*'],
          body: [
            [
              { text: 'Shareholder Health Insurance', style: 'tableHeader' },
              { text: 'Employee Health Insurance', style: 'tableHeader' },
              { text: 'Employee Health Doc', style: 'tableHeader' },
              { text: 'Employer Health Insurance', style: 'tableHeader' },
              { text: 'Employer Health Doc', style: 'tableHeader' },
            ],
            [
              { text: emp.shareholderHealthInsurance, style: 'tableRow' },
              { text: emp.employeePortionHealthInsurance, style: 'tableRow' },
              { text: emp.employeePortionDocumentUrl.url ? 'Check Email for Document' : 'N/A', style: 'tableRow' },
              { text: emp.employerPortionHealthInsurance, style: 'tableRow' },
              { text: emp.employerPortionDocumentUrl.url ? 'Check Email for Document' : 'N/A', style: 'tableRow' },
            ]
          ]
        }
      };
      this.dd.content.push(healthTable);

      this.dd.content.push({ text: 'Notes', style: 'additionalPayHeader' });
      const notesTable: any = {
        table: {
          widths: ['*'],
          body: [
            [
              { text: emp.notes, style: 'tableRow' }
            ]
          ]
        }
      };
      this.dd.content.push(notesTable);

      this.dd.content.push({ text: '', style: 'gap' });
    });
  }

  dec(val) {
    return this.dp.transform(val, '1.2-2');
  }
}
