import { Directive, ElementRef, OnInit, HostListener } from '@angular/core';
import { CurrencyPipe } from '@angular/common';

@Directive({
  selector: '[appCurrency]'
})
export class CurrencyDirective implements OnInit {

  private el: HTMLInputElement;

  constructor(
    private elRef: ElementRef,
    private cur: CurrencyPipe
  ) {
    this.el = elRef.nativeElement;
  }

  ngOnInit() {
    this.el.value = this.cur.transform(this.el.value, 'USD', 'symbol', '1.0-0');
  }

  @HostListener('focus', ['$event.target.value'])
  onFocus(value) {
    // this.el.value = this.cur.transform() // opossite of transform
  }

  @HostListener('blur', ['$event.target.value'])
  onBlur(value) {
    // this.el.value = this.cur.transform(value);
  }

}
