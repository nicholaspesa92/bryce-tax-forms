import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PayrollService } from './../ng-services/payroll.service';
import { AuthService } from './../ng-services/auth.service';

import * as pdfMake from 'pdfmake/build/pdfmake';
import * as pdfFonts from 'pdfmake/build/vfs_fonts';

@Component({
  selector: 'app-review',
  templateUrl: './review.component.html',
  styleUrls: ['./review.component.scss']
})
export class ReviewComponent implements OnInit {

  pd: any;
  ed: any;

  dd: any;
  user: any;

  constructor(
    private router: Router,
    private ps: PayrollService,
    private as: AuthService
  ) {
    pdfMake.vfs = pdfFonts.pdfMake.vfs;
  }

  ngOnInit() {
    this.pd = this.ps.payrollData;
    this.ed = this.ps.employeeData;
  }

  getDoc() {
    this.dd = this.ps.buildDocument(this.pd, this.ed);
    const doc = pdfMake.createPdf(this.dd);
    doc.getDataUrl((dUrl) => {
      this.upload(dUrl);
    });
  }

  async upload(dUrl) {
    const url = await this.ps.uploadDataUrl(dUrl, `submissions/${this.user.uid}`);
    console.log(url);
  }

}
